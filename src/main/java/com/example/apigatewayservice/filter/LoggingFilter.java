package com.example.apigatewayservice.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

/**
 * Logging하는 Grobal Filter 작성
 * 
 * 1.  AbstractGatewayFilterFactory상속
 * 2.  inner static class로 Config 정의 - configuration properties member variable선언 
 * 3.  default constructor : super(Config.class) 전달
 * 4.  apply override - request, response filter 작성
 */
@Component
public class LoggingFilter extends AbstractGatewayFilterFactory<LoggingFilter.Config> {

	Logger log = LoggerFactory.getLogger(LoggingFilter.class);
//	 AbstractGatewayFilterFactory에 config 전달
	public LoggingFilter() {
		super(Config.class);
	}


	@Override
	public GatewayFilter apply(Config config) {
		return (exchange, chain)->{
		    ServerHttpRequest request =   exchange.getRequest();
		    ServerHttpResponse response = exchange.getResponse();
		    //request filter
		    log.info("LoggingFilter baseMessage{} ", config.getBaseMessage());
		    if(config.isPreLogger()) {
		    	log.info("LoggingFilter Pre Filter {} ", request.getId());
		    }
		   return  chain.filter(exchange).then(Mono.fromRunnable(() -> { //response filter
             if(config.isPostLogger()) {
            	 log.info("LoggingFilter Post Filter {} ", response.getStatusCode());
             }
           })); 
		}; 
	}
	
/**
 *  application.yml파일의 args 데이터 setting
 */
	 public static class Config {
	        //Put the configuration properties for your filter here
		 private String baseMessage;
		 private boolean preLogger;
		 private boolean postLogger;
		public String getBaseMessage() {
			return baseMessage;
		}
		public void setBaseMessage(String baseMessage) {
			this.baseMessage = baseMessage;
		}
		public boolean isPreLogger() {
			return preLogger;
		}
		public void setPreLogger(boolean preLogger) {
			this.preLogger = preLogger;
		}
		public boolean isPostLogger() {
			return postLogger;
		}
		public void setPostLogger(boolean postLogger) {
			this.postLogger = postLogger;
		}
		 
		
	 }

}
