package com.example.apigatewayservice.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import com.example.apigatewayservice.exception.UnAthenticationException;

import io.jsonwebtoken.Jwts;
import reactor.core.publisher.Mono;

/**
 * 인증 Filter 작성
 * 
 * 1.  AbstractGatewayFilterFactory상속
 * 2.  inner static class로 Config 정의 - configuration properties member variable선언 
 * 3.  default constructor : super(Config.class) 전달
 * 4.  apply override - request access-token, userId check->JWT 점검 filter 작성
 */
@Component
public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

	Logger log = LoggerFactory.getLogger(AuthenticationFilter.class);
//	 AbstractGatewayFilterFactory에 config 전달
	public AuthenticationFilter() {
		super(Config.class);
	}


	@Override
	public GatewayFilter apply(Config config) {
		return (exchange, chain)->{
		    ServerHttpRequest request =   exchange.getRequest();
		    ServerHttpResponse response = exchange.getResponse();
		    
		    
		    String accessToken = request.getHeaders().getFirst("access-token");
		    String userId = request.getHeaders().getFirst("userId");
		    //jwt 점검
		    String subject =  Jwts.parser().setSigningKey(config.getSecret())
				   			 .parseClaimsJws(accessToken)
							 .getBody()
							 .getSubject();
		    
		    //request filter - token 유효성 검증
		    if(accessToken == null || userId == null || 
		    		 accessToken.isEmpty() || userId.isEmpty() || !userId.equals(subject)){
		    		log.error("유효하지 않은 token");
		    	throw new UnAthenticationException();
		    }
		    
		    log.info("사용가능한 토큰 {}", userId);
		    //인증 - APIGateway에서 token관리?  각 서비스에서 token 보유
//		    request.mutate()
//		    	   .header("userId", subject)
//		    	   .build();
		    
		    return chain.filter(exchange);
		    
		}; 
	}
	
/**
 *  application.yml파일의 args 데이터 setting
 */
	 public static class Config {
	        //Put the configuration properties for your filter here
		 private String secret;

		public String getSecret() {
			return secret;
		}

		public void setSecret(String secret) {
			this.secret = secret;
		}
	
		
	 }

}
